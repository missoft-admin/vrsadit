const express = require('express')
var request = require('request');
var cors = require('cors')
const app = express()
var router = express.Router();

app.use(cors())

app.get('/', function (req, res) {
    res.send('404')
})

app.param('barcode', function(req,res,next,barcode){
    req.barcode = barcode;
    next();
});

app.param('nominal', function(req,res,next,nominal){
    req.nominal = nominal;
    next();
});

app.param('location', function(req,res,next,location){
    req.location = location;
    next();
});

app.get('/inquiry/:barcode',function(req,res){
    var options = {
      url: 'http://192.168.1.10/transretail_vrs/Enquery/rest_check_enquiry',
      method: 'POST',
      json: {
        "barcode": req.barcode
      }
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        res.send(body);
        request.post({url:'http://192.168.1.10/transretail_vrs/log/set_logging', json:{
          "activity":"Inquiry",
          "description":"Check Inquiry",
          "location":"1"
        }}, function(err,httpResponse,body){
          console.log("Console Activity : Inquiry ("+ req.barcode +")");
        })
      }else{
        request.post({url:'http://192.168.1.10/transretail_vrs/log/set_logging', json:{
          "activity":"Inquiry",
          "description":"Check Inquiry ( Failed )",
          "location":"1"
        }}, function(err,httpResponse,body){
          console.log("Console Activity : Inquiry Failed Server Not Response");
        })
        console.log("Console Activity : Inquiry Error Check Log");
      }
    });

})

app.get("/redeem/:barcode/:nominal/:location",function(req,res){
    var options = {
      uri: 'http://192.168.1.10/transretail_vrs/Redeem/rest_check_redeem',
      method: 'POST',
      json: {
        "barcode": req.barcode,
        "nominal": req.nominal,
        "id_outlet": req.location
      }
    };
    var dataJson = "Hello World";
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        res.send(body);
        request.post({url:'http://192.168.1.10/transretail_vrs/log/set_logging', json:{
          "activity":"Redeem",
          "description":"Redeemed ( Success Full )",
          "location":"1"
        }}, function(err,httpResponse,body){
          console.log("Console Activity : Redeem");
        })
      }else{
        request.post({url:'http://192.168.1.10/transretail_vrs/log/set_logging', json:{
          "activity":"Redeem",
          "description":"Redeem ( Failed )",
          "location":"1"
        }}, function(err,httpResponse,body){
          console.log("Console Activity : Redeem Error Check Log");
        })
      }
    });
});

app.get("/reactive/:barcode",function(req,res){
    var options = {
      uri: 'http://192.168.1.10/transretail_vrs/Redeem/rest_reactive',
      method: 'POST',
      json: {
        "barcode": req.barcode
      }
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        res.send(body);
      }
    });
});

app.listen(3000, function () {
	console.log('Carrfour Middleware Application Run At Port : 3000')
})
